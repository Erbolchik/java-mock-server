package org.mockserver.model;

import java.util.ArrayList;

public class Deposit {
    String employer;
    ArrayList<PositionVo> positions;

    public Deposit(String employer, ArrayList<PositionVo> positions) {
        this.employer = employer;
        this.positions = positions;
    }

    public Deposit() {
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public ArrayList<PositionVo> getPositions() {
        return positions;
    }

    public void setPositions(ArrayList<PositionVo> positions) {
        this.positions = positions;
    }
}
