package org.mockserver.model;

import javax.persistence.*;

@Entity
@Table(name = "ApproveSubmitOffer")
public class ApproveSubmitOffer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String offerCode;
    private String offerRelipCode;
    private int loanLength;
    private int amount;


    public ApproveSubmitOffer(String offerCode, String offerRelipCode, int loanLength, int amount) {
        this.offerCode = offerCode;
        this.offerRelipCode = offerRelipCode;
        this.loanLength = loanLength;
        this.amount = amount;
    }

    public ApproveSubmitOffer() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOfferCode() {
        return offerCode;
    }

    public void setOfferCode(String offerCode) {
        this.offerCode = offerCode;
    }

    public String getOfferRelipCode() {
        return offerRelipCode;
    }

    public void setOfferRelipCode(String offerRelipCode) {
        this.offerRelipCode = offerRelipCode;
    }

    public int getLoanLength() {
        return loanLength;
    }

    public void setLoanLength(int loanLength) {
        this.loanLength = loanLength;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
