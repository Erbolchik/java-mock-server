package org.mockserver.model;


import javax.persistence.*;

@Entity
@Table(name = "CardOwnerData")
public class CardOwnerData {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private String accountType;
    private int accountState;
    private int clientType;
    private boolean successfull;
    private boolean tooManyAttempts;

    public CardOwnerData(String name, String accountType, int accountState,
                         int clientType, boolean successfull, boolean tooManyAttempts) {
        this.name = name;
        this.accountType = accountType;
        this.accountState = accountState;
        this.clientType = clientType;
        this.successfull = successfull;
        this.tooManyAttempts = tooManyAttempts;
    }

    public CardOwnerData() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public int getAccountState() {
        return accountState;
    }

    public void setAccountState(int accountState) {
        this.accountState = accountState;
    }

    public int getClientType() {
        return clientType;
    }

    public void setClientType(int clientType) {
        this.clientType = clientType;
    }

    public boolean isSuccessfull() {
        return successfull;
    }

    public void setSuccessfull(boolean successfull) {
        this.successfull = successfull;
    }

    public boolean isTooManyAttempts() {
        return tooManyAttempts;
    }

    public void setTooManyAttempts(boolean tooManyAttempts) {
        this.tooManyAttempts = tooManyAttempts;
    }

}
