package org.mockserver.model;

public class PositionVo{
    public String id;
    public String code;

    public PositionVo(String id, String code) {
        this.id = id;
        this.code = code;
    }

    public PositionVo() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
