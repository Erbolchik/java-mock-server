package org.mockserver.model;

import javax.persistence.*;

@Entity
@Table(name="LoanPreApprovePassword")
public class LoanPreApprovePassword {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String password;

    public LoanPreApprovePassword(String password) {
        this.password = password;
    }
    public LoanPreApprovePassword() { }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
