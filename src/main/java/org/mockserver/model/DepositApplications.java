package org.mockserver.model;

import javax.persistence.*;

@Entity
@Table(name = "DepositApplications")
public class DepositApplications {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String applicationTypeCode;
    private String applicationTypeText;
    private int registrationDate;
    private String statusCode;
    private String statusText;
    private String depositType;
    private String errorText;
    private String bot;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getApplicationTypeCode() {
        return applicationTypeCode;
    }

    public void setApplicationTypeCode(String applicationTypeCode) {
        this.applicationTypeCode = applicationTypeCode;
    }

    public String getApplicationTypeText() {
        return applicationTypeText;
    }

    public void setApplicationTypeText(String applicationTypeText) {
        this.applicationTypeText = applicationTypeText;
    }

    public int getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(int registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public String getDepositType() {
        return depositType;
    }

    public void setDepositType(String depositType) {
        this.depositType = depositType;
    }

    public String getErrorText() {
        return errorText;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    public String getBot() {
        return bot;
    }

    public void setBot(String bot) {
        this.bot = bot;
    }

    public DepositApplications(String applicationTypeCode, String applicationTypeText, int registrationDate,
                               String statusCode, String statusText, String depositType, String errorText, String bot) {
        this.applicationTypeCode = applicationTypeCode;
        this.applicationTypeText = applicationTypeText;
        this.registrationDate = registrationDate;
        this.statusCode = statusCode;
        this.statusText = statusText;
        this.depositType = depositType;
        this.errorText = errorText;
        this.bot = bot;
    }

    public DepositApplications() {
    }

    ;


}

