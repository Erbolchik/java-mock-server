package org.mockserver.controller;

import org.mockserver.model.DepositApplications;
import org.mockserver.repository.DepositApplicationsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/DepositApplications")
public class DepositApplicationsController {
    @Autowired
    DepositApplicationsRepository depositApplicationsRepository;

    @GetMapping("/get")
    public List<DepositApplications> getAllDepositApplications() {
        return depositApplicationsRepository.findAll();
    }
}
