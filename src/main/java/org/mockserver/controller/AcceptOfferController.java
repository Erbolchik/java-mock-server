package org.mockserver.controller;

import org.mockserver.model.ApproveSubmitOffer;
import org.mockserver.repository.AcceptOfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/acceptOffer")
public class AcceptOfferController {
    @Autowired
    AcceptOfferRepository acceptOfferRepository;

    @PostMapping("/post")
    public ResponseEntity<String> createOfferList(@Valid @RequestBody ApproveSubmitOffer approveSubmitOffer) {

        acceptOfferRepository.save(approveSubmitOffer);
        return ResponseEntity.ok("created");
    }

    @GetMapping("/get")
    public List<ApproveSubmitOffer> getAllOffers() {
        return acceptOfferRepository.findAll();
    }

}
