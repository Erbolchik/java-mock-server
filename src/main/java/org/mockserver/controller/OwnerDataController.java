package org.mockserver.controller;

import org.mockserver.model.CardOwnerData;
import org.mockserver.model.DepositApplications;
import org.mockserver.repository.CardOwnerDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/ownerData")
public class OwnerDataController {
    @Autowired
    CardOwnerDataRepository cardOwnerDataRepository;

    @GetMapping("/get")
    public List<CardOwnerData> getAllDepositApplications() {
        return cardOwnerDataRepository.findAll();
    }
}
