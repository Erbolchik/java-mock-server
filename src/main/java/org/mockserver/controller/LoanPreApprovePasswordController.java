package org.mockserver.controller;

import org.mockserver.exception.ResourceNotFoundException;
import org.mockserver.model.LoanPreApprovePassword;
import org.mockserver.repository.LoanPreApprovePasswordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/LoanPreApprovePassword")
public class LoanPreApprovePasswordController {
    @Autowired
    LoanPreApprovePasswordRepository loanPreApprovePasswordRepository;

    @GetMapping("/get")
    public List<LoanPreApprovePassword> getAllPasswords() {
        return loanPreApprovePasswordRepository.findAll();
    }

    @PostMapping("/post")
    public ResponseEntity<String> savePassword(@Valid @RequestBody LoanPreApprovePassword newPassword) {
        loanPreApprovePasswordRepository.save(newPassword);
        return ResponseEntity.ok("created");
    }

    @PutMapping("/put/{id}")
    public ResponseEntity<String> changePassword(@PathVariable(value = "id") long id, @Valid
    @RequestBody LoanPreApprovePassword newPassword) throws ResourceNotFoundException{

        LoanPreApprovePassword loanPreApprovePassword = loanPreApprovePasswordRepository.findById(id).orElseThrow(
                ()->new ResourceNotFoundException("password not found this id " + id));

        loanPreApprovePassword.setPassword(newPassword.getPassword());
        loanPreApprovePasswordRepository.save(loanPreApprovePassword);
        return ResponseEntity.ok("password changed");
    }


}
