package org.mockserver.repository;

import org.mockserver.model.LoanPreApprovePassword;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoanPreApprovePasswordRepository extends JpaRepository<LoanPreApprovePassword,Long> {
}
