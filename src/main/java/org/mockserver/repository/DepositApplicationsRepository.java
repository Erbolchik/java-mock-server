package org.mockserver.repository;

import org.mockserver.model.DepositApplications;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepositApplicationsRepository extends JpaRepository<DepositApplications,Long> {
}
