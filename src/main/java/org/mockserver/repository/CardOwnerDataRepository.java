package org.mockserver.repository;

import org.mockserver.model.CardOwnerData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CardOwnerDataRepository extends JpaRepository<CardOwnerData,Long> {
}
