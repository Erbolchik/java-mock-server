package org.mockserver.repository;

import org.mockserver.model.ApproveSubmitOffer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AcceptOfferRepository extends JpaRepository<ApproveSubmitOffer,Long> {

}
